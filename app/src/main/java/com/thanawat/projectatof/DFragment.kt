package com.thanawat.projectatof

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.projectatof.databinding.FragmentCBinding
import com.thanawat.projectatof.databinding.FragmentDBinding


class DFragment : Fragment() {
        private var _binding: FragmentDBinding? = null
        private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDBinding.inflate(inflater,container,false)
        var view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnGoE?.setOnClickListener {
            val action = DFragmentDirections.actionDFragmentToEFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoC?.setOnClickListener {
            val action = DFragmentDirections.actionDFragmentToCFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}