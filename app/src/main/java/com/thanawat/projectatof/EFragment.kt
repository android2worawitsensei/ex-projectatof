package com.thanawat.projectatof

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.projectatof.databinding.FragmentBBinding
import com.thanawat.projectatof.databinding.FragmentEBinding

class EFragment : Fragment() {
    private var _binding: FragmentEBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEBinding.inflate(inflater,container,false)
        var view = binding?.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnGoF?.setOnClickListener {
            val action = EFragmentDirections.actionEFragmentToFFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoD?.setOnClickListener {
            val action = EFragmentDirections.actionEFragmentToDFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {

    }
}