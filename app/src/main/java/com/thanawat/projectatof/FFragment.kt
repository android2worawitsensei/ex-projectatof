package com.thanawat.projectatof

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.projectatof.databinding.FragmentBBinding
import com.thanawat.projectatof.databinding.FragmentFBinding


class FFragment : Fragment() {
    private var _binding: FragmentFBinding? = null
    private val binding get() = _binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFBinding.inflate(inflater,container,false)
        var view = binding?.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnGoA?.setOnClickListener {
            val action = FFragmentDirections.actionFFragmentToAFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoE?.setOnClickListener {
            val action = FFragmentDirections.actionFFragmentToEFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}