package com.thanawat.projectatof

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.thanawat.projectatof.databinding.FragmentCBinding

class CFragment : Fragment() {
    private var _binding: FragmentCBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCBinding.inflate(inflater,container,false)
        var view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnGoD?.setOnClickListener {
            val action = CFragmentDirections.actionCFragmentToDFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnGoB?.setOnClickListener {
            val action = CFragmentDirections.actionCFragmentToBFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {

    }
}